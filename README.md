# EvolveUX

EvolveUX is a fast structured feedback protocol for user experience designers to constructively critique each others' work. The feedback helps them improve their designs, and get them ready for a follow-up feedback session.

## Table of Contents

- [Background](#background)
- [Interaction Protocol or API](#interaction-protocol-or-API)
- [Install](#install)
- [Usage](#usage)
- [Contributing](#contributing)
- [License & Attribution](#license-attribution)

## Background

EvolveUX is the 4th iteration of a feedback protocol for makers/designers, where multiple elements have stabilized. Prior to EvolveUx we experimented with the ExposeUX series at OVH, the ProtoHack series at Salon 1981 (the local ImpactHub), and the UX Feedback sessions at Notman House. (all had feedback forms, an emcee, etc.)

## Interaction Protocol or API

Today's segment-by-segment protocol consists of 3 stages:

- **the hashtag #** - what is it that just happened
- **the heart ❤** - what is there to appreciate about what just happened
- **the delta Δ** - what to consider improving

This repeats several times in the evening depending on how many people attending have artifacts to present.

Send an MR to tweak the protocol.

## Install

The minimal requirements: 
- 2+ people or systems need to interact,
- a scheduled time, and
- a place (can be online)

## Usage

1. Share an artifact and declare the specific type of feedback you need
2. Engage the attendees to evaluate your presentation in #❤Δ format.

## Contributing

Feel free to dive in! [Open an issue](https://gitlab.com/samergo/agents/evolveux/issues/new) or submit MRs.

EvolveUX follows the [Contributor Covenant](http://contributor-covenant.org/version/1/3/0/) Code of Conduct.

## License & Attribution

The EvolveUX protocol is available under the [Creative Commons CC0 1.0 License](https://creativecommons.org/publicdomain/zero/1.0/), meaning you are free to use it for any purpose, commercial or non-commercial, without any attribution back to the original authors (public domain).
